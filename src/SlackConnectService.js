import {ApplicationService, TraceUtils} from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { AccountReplacer } from './AccountReplacer';
import { InstantMessageReplacer } from "./InstantMessageReplacer";
import { slackConnectRouter} from "./routers/slackConnectRouter";
import onHeaders from "on-headers";
import {WebClient} from "@slack/web-api";

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex((item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex((item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

function noCache() {
    return (req, res, next) => {
        onHeaders(res, ()=> {
            res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.setHeader('Pragma', 'no-cache');
            res.setHeader('Expires', 0);
            // remove express js etag header
            res.removeHeader('ETag');
        });
        return next();
    }
}

export class SlackConnectService extends ApplicationService {
    constructor(app) {

        new AccountReplacer(app).apply();

        new InstantMessageReplacer(app).apply();



        super(app);

        this.options = app.getConfiguration().getSourceAt('settings/universis/slack-connect');

        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // use router
                    container.use('/services/slack', noCache(), slackConnectRouter(app));
                    // get container router
                    const router = container._router;
                    // find before position
                    const before = router.stack.find((item) => {
                        return item.name === 'dataContextMiddleware';
                    });
                    if (before == null) {
                        // do nothing
                        return;
                    }
                    const insert = router.stack[router.stack.length - 1];
                    // re-index router
                    insertRouterBefore(router, before, insert);

                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        } else {
            // container is missing
            TraceUtils.warn('SlackConnectService', 'Application container is missing. Discord connect service endpoint will be unavailable.');
        }
    }

    sendDirectMessage(recipient,message)
    {

        if (!recipient.slackAccount || !recipient.slackAccount.identifier)
        {
            console.log("The recipient does not have a slack account connected.");
            return;
        }

        const {WebClient} = require('@slack/web-api');
        const slackClient = new WebClient();

        console.log(this.options.bot_user_token)
        slackClient.chat.postMessage({
                token : this.options && this.options.bot_user_token,
                channel : recipient.slackAccount.identifier,
                text : message
        }).catch(err => {
            console.log(err);
        })

    }
}
