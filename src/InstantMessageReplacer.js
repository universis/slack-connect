import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class InstantMessageReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('InstantMessage');
        model.eventListeners = model.eventListeners || [];
        model.eventListeners.push({
            type: path.resolve(__dirname, './listeners/OnInsertInstantMessage')
        });
        schemaLoader.setModelDefinition(model);
    }

}

export {
    InstantMessageReplacer
}
