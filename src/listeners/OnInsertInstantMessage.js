import {DataError} from '@themost/common';
import { DataObjectState } from '@themost/data';
import {SlackConnectService} from "../SlackConnectService";
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
    //
} 

async function afterSaveAsync(event) {
    //
    if (event.state === DataObjectState.Insert)
    {
        const context = event.model.context;

        const channel = await context.model('MessagingChannel').find(event.target.recipient).expand({ name: 'members', options: { $expand: 'slackAccount' } }).silent().getItem();

        let recipients = [];
        if (channel)
        {
            let i = 0;
            for (const member of channel.members)
            {
                if (member.id !== event.target.owner)
                {
                    recipients[i] = member;
                    i++;
                }
            }
        }

        for (const recipient of recipients){
            context.getApplication().getService(SlackConnectService).sendDirectMessage(recipient,event.target.body);
        }
    }

} 

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}